import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="verify-rill-collection-sid", # Replace with your own username
    version="0.0.1",
    author="Sid",
    author_email="sidharth.g@swiggy.in",
    description="Verification",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/sidharth_g/sparkjobsverification",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: ",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)