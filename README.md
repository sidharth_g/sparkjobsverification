Build:
```
mkdir ./dist
cp ./src/main.py ./dist
cd ./src && zip -x main.py -r ../dist/jobs.zip . && cd ..
```


Submit:
```
export PYSPARK_PYTHON=python3
cd dist && spark-submit --py-files jobs.zip main.py --job verify1 --help
```

Example:
```
export PYSPARK_PYTHON=python3
cd dist
spark-submit --py-files jobs.zip main.py --job verify1  --path-a data-platform-json/json_logs/daily/awz_s3_ListingV4Response/dt=2020-02-19/hr=13/ --path-b data-platform-json/json_logs/daily/rill_o2a_within_15/dt=2020-02-19/hr=13/ --unique-fields request_id timestamp > out.log
```