import argparse
from typing import Any, List, Optional

from .helper import Helpers, ComparisonUtil


class ArgParse(object):

    @staticmethod
    def get_parser(parser: Optional = None) -> 'argparse.ArgumentParser':
        if parser is None:
            parser = argparse.ArgumentParser(description='Validate events')
        parser.add_argument('-f', '--fields', nargs='+', help='Fields to use (space-separated)')
        parser.add_argument('-u', '--unique-fields', nargs='+', help='Unique fields to use (space-separated)',
                            required=True)
        parser.add_argument('-a', '--path-a', help='First path', required=True)
        parser.add_argument('-b', '--path-b', help='Second path', required=True)

        return parser


def execute(spark: Any, sc: Any, hadoop_conf: Any, path_a: str, path_b: str, fields: List[str],
            unique_fields: List[str]):
    assert len(fields) == 0 or len(set(unique_fields) - set(fields)) == 0
    _ = sc  # NOOP
    _ = hadoop_conf  # NOOP
    print('Getting dataframes..')
    df_a = Helpers.get_df(spark=spark, path=path_a)
    df_b = Helpers.get_df(spark=spark, path=path_b)

    print('1. Checking counts..')
    counts_equal = ComparisonUtil.counts_equal(df_a=df_a, df_b=df_b)
    print("Counts equal: {}".format(counts_equal))

    print('2. Checking schemas..')
    schemas_equal = ComparisonUtil.schemas_equal(df_a=df_a, df_b=df_b)
    print("Schemas equal: {}".format(schemas_equal))

    print("3. Checking dataframe difference..")
    difference_nil = ComparisonUtil.difference_nil(df_a=df_a, df_b=df_b)
    print("No differences: {}".format(difference_nil))



def update_parser(parser: 'argparse.ArgumentParser') -> 'argparse.ArgumentParser':
    return ArgParse.get_parser(parser=parser)


def analyze(spark, sc, hadoop_conf, parsed_args: Any):
    execute(
        spark=spark,
        sc=sc,
        hadoop_conf=hadoop_conf,
        path_a=parsed_args.path_a,
        path_b=parsed_args.path_b,
        fields=parsed_args.fields if parsed_args.fields else [],
        unique_fields=parsed_args.unique_fields,
    )
