from typing import Optional, Any


class Helpers(object):

    @staticmethod
    def get_df(spark, path: str, file_type: Optional[str] = 'json') -> Any:
        readers_map = {
            'json': spark.read.json,
            'orc': spark.read.orc,
        }

        assert type(path) is str
        reader = readers_map[file_type]
        df = reader(path)
        return df


class ComparisonUtil(object):
    def __init__(self) -> None:
        pass

    @staticmethod
    def counts_equal(df_a: Any, df_b: Any) -> bool:
        count_a = df_a.count()
        count_b = df_b.count()
        print('count_a: {}'.format(count_a))
        print('count_b: {}'.format(count_b))
        return count_a == count_b

    @staticmethod
    def schemas_equal(df_a: Any, df_b: Any) -> bool:
        print('Schema (df_a):')
        print(df_a.schema)
        print('Schema (df_b):')
        print(df_b.schema)
        return df_a.schema == df_b.schema

    @staticmethod
    def difference_nil(df_a: Any, df_b: Any) -> bool:
        schemas_equal = ComparisonUtil.schemas_equal(df_a=df_a, df_b=df_b)
        if not schemas_equal:
            print('Skipping difference check since schemas are not equal')
            return False

        diff_a_b = df_a.subtract(df_b).count()
        print("Difference count (A-B): {}".format(diff_a_b))
        diff_b_a = df_b.subtract(df_a).count()
        print("Difference count (B-A): {}".format(diff_b_a))

        return df_a == 0 and diff_b_a == 0
