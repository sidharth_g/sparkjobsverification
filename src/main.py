# Reference: https://developerzen.com/best-practices-writing-production-grade-pyspark-jobs-cb688ac4d20f

import argparse
import importlib
import os
import sys

from pyspark.sql import SparkSession

if os.path.exists('jobs.zip'):
    sys.path.insert(0, 'jobs.zip')
else:
    sys.path.insert(0, './jobs')

parser = argparse.ArgumentParser()
parser.add_argument('--job', type=str, required=True)
# parser.add_argument('--job-args', nargs='*')
# args = parser.parse_args()
args, unknown = parser.parse_known_args()
print('===========================')
print(args)

spark = SparkSession.builder.appName('awz_s3_ListingV4Response').getOrCreate()

# TODO: Remove keys
access_id = ""
access_key = ""
sc = spark.sparkContext
hadoop_conf = sc._jsc.hadoopConfiguration()
hadoop_conf.set("fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
hadoop_conf.set("fs.s3n.awsAccessKeyId", access_id)
hadoop_conf.set("fs.s3n.awsSecretAccessKey", access_key)

spark = SparkSession.builder.appName(args.job).getOrCreate()
# sc = pyspark.SparkContext(appName=args.job)
job_module = importlib.import_module('jobs.%s' % args.job)
parser = job_module.update_parser(parser)
args_updated = parser.parse_args()
job_module.analyze(spark, sc, hadoop_conf, args_updated)
